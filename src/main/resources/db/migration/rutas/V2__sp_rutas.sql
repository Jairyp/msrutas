

DELIMITER //

CREATE PROCEDURE GetAllRutas()
BEGIN
    SELECT *  FROM ruta;
END //



CREATE PROCEDURE GetAllRutasById (IN id_Ruta INT)
BEGIN
    SELECT * FROM ruta WHERE idRuta = id_Ruta;
END //

CREATE PROCEDURE RutasInsert (IN p_idRuta int(11),
                            IN p_nombre varchar(120),
                            IN p_parada varchar(120),
                            IN p_latitud FLOAT(10,6),
                            IN p_longitud FLOAT(10,6))
BEGIN
    insert into ruta (idRuta,nombre,parada,latitud,longitud) values(p_idRuta,p_nombre,p_parada,p_latitud,p_longitud);
END //

CREATE PROCEDURE RutasUpdate (IN p_id INT,
                            IN p_idRuta INT,
                            IN p_nombre varchar(120),
                            IN p_parada varchar(120),
                            IN p_latitud FLOAT(10,6),
                            IN p_longitud FLOAT(10,6))
BEGIN
    update ruta set idRuta=p_idRuta,nombre=p_nombre,parada=p_parada,latitud=p_latitud,longitud=p_longitud
    where id = p_id;
END //

CREATE PROCEDURE RutasDelete (IN p_id INT)
BEGIN
    delete from ruta where id = p_id;
END //

DELIMITER ;

