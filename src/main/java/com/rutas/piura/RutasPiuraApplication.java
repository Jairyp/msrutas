package com.rutas.piura;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RutasPiuraApplication {

	public static void main(String[] args) {
		SpringApplication.run(RutasPiuraApplication.class, args);
	}

}
