package com.rutas.piura.controller.web.dto;

import com.rutas.piura.core.domain.Ruta;

import java.util.List;

public class RutasWebDto {

    private List<Ruta> rutas;

    public RutasWebDto(List<Ruta> rutas) {
        this.rutas = rutas;
    }

    public List<Ruta> getRutas() {
        return rutas;
    }

    public void setRutas(List<Ruta> rutas) {
        this.rutas = rutas;
    }

}
