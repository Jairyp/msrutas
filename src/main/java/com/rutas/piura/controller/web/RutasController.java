package com.rutas.piura.controller.web;

import com.rutas.piura.controller.web.dto.RutaWebDto;
import com.rutas.piura.controller.web.dto.RutasWebDto;
import com.rutas.piura.core.domain.Ruta;
import com.rutas.piura.service.RutaService;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.util.List;

@RestController
@RequestMapping("/rutas")
public class RutasController {

    private RutaService rutaService;

    public RutasController(RutaService rutaService) {
        this.rutaService = rutaService;
    }

    @GetMapping
    public HttpEntity<RutasWebDto> getList() {

        List<Ruta> rutas;
        try{
            rutas =  rutaService.getList();
            RutasWebDto rutasWebDto = new RutasWebDto(rutas);
            return new ResponseEntity<>(rutasWebDto, HttpStatus.OK);
        }catch (Exception ex){
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }


    }

    @GetMapping("/{idRuta}")
    public HttpEntity<RutasWebDto> getById(@PathVariable("idRuta") int id) {

        List<Ruta> rutas = rutaService.getById(id);

        if(!rutas.isEmpty()){
            RutasWebDto rutasWebDto = new RutasWebDto(rutas);
            return new ResponseEntity<>(rutasWebDto, HttpStatus.OK);
        }else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }


    @PostMapping
    public HttpEntity<RutaWebDto> create(@RequestBody RutaWebDto rutaWebDto) {
            rutaService.save(rutaWebDto.getRuta());
            return ResponseEntity.ok().build();
    }

    @PutMapping("/{idRuta}")
    public HttpEntity<RutaWebDto> update(@PathVariable(name = "idRuta") int id,
                                         @RequestBody RutaWebDto rutaWebDto) {
        rutaWebDto.getRuta().setIdRuta(id);
        rutaService.update(rutaWebDto.getRuta());
        return ResponseEntity.ok().build();
    }

    @DeleteMapping("/{idRuta}")
    public HttpEntity<Void> delete(
            @PathVariable(name = "idRuta") int id) {

        Ruta ruta =new Ruta();
        ruta.setIdRuta(id);

        rutaService.delete(ruta);
        return ResponseEntity.ok().build();


    }

}
