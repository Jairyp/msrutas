package com.rutas.piura.service;

import com.rutas.piura.core.domain.Ruta;

import java.util.List;

public interface RutaService {

    List<Ruta> getList();
    List<Ruta> getById(int id);
    void save(Ruta ruta);
    void update(Ruta ruta) ;
    void delete(Ruta ruta);

}
