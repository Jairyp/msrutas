package com.rutas.piura.service;

import com.rutas.piura.core.domain.Ruta;
import com.rutas.piura.repository.RutaRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RutaServiceImpl implements RutaService{

    private RutaRepository rutaRepository;

    public RutaServiceImpl(RutaRepository rutaRepository) {
        this.rutaRepository = rutaRepository;
    }

    @Override
    public List<Ruta> getList() {
        return rutaRepository.getList();
    }

    @Override
    public List<Ruta> getById(int id) {
        return rutaRepository.getById(id);
    }

    @Override
    public void save(Ruta ruta) {
        rutaRepository.save(ruta);
    }

    @Override
    public void update(Ruta ruta) {
        rutaRepository.update(ruta);
    }

    @Override
    public void delete(Ruta ruta) {
        rutaRepository.delete(ruta);
    }
}
