package com.rutas.piura.repository;

import com.rutas.piura.core.domain.Ruta;

import java.util.List;

public interface RutaRepository {

    List<Ruta> getById(int id);
    List<Ruta> getList();
    int save(Ruta ruta);
    void update(Ruta ruta) ;
    void delete(Ruta ruta);

}
