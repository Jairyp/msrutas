package com.rutas.piura.repository;

import com.rutas.piura.core.domain.Ruta;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;

@Component
public class RutaRowMapper implements RowMapper<Ruta>{

    @Override
    public Ruta mapRow(ResultSet rs, int i) throws SQLException {
        Ruta ruta = new Ruta();
        ruta.setId(rs.getInt("id"));
        ruta.setIdRuta(rs.getInt("idRuta"));
        ruta.setNombre(rs.getString("nombre"));
        ruta.setParada(rs.getString("parada"));
        ruta.setLatitud(rs.getFloat("latitud"));
        ruta.setLongitud(rs.getFloat("longitud"));
        ruta.setFlagTipoRuta(rs.getInt("flagTipoRuta"));
        return ruta;
    }
}
