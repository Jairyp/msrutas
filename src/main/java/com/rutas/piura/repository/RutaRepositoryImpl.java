package com.rutas.piura.repository;

import com.rutas.piura.core.domain.Ruta;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class RutaRepositoryImpl implements RutaRepository {

    private static final Logger log = LoggerFactory.getLogger(RutaRepositoryImpl.class);


    private final JdbcTemplate jdbcTemplate;
    private final RutaRowMapper rutaRowMapper;

    public RutaRepositoryImpl(JdbcTemplate jdbcTemplate, RutaRowMapper rutaRowMapper) {
        this.jdbcTemplate = jdbcTemplate;
        this.rutaRowMapper = rutaRowMapper;
    }

    @Override
    public List<Ruta> getById(int id) {
        List<Ruta> ruta = jdbcTemplate.query("call GetAllRutasById(?)", new Object[] { id }, rutaRowMapper);
        return ruta;
    }

    @Override
    public List<Ruta> getList() {
        List<Ruta> list = jdbcTemplate.query("call GetAllRutas",rutaRowMapper);
        return list;
    }

    @Override
    public int save(Ruta ruta) {

        return jdbcTemplate.update("call RutasInsert(?,?,?,?,?);",new Object[] { ruta.getIdRuta(),
                ruta.getNombre(),
                ruta.getParada(),
                ruta.getLatitud(),
                ruta.getLongitud()});
    }

    @Override
    public void update(Ruta ruta) {
        jdbcTemplate.update("call RutasUpdate(?,?,?,?,?,?);",new Object[] { ruta.getId(),
                ruta.getIdRuta(),
                ruta.getNombre(),
                ruta.getParada(),
                ruta.getLatitud(),
                ruta.getLongitud()});
    }

    @Override
    public void delete(Ruta ruta) {
        jdbcTemplate.update("call RutasDelete(?);",new Object[] { ruta.getId()});
    }

}
